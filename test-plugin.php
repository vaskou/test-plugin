<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Test plugin
 * Plugin URI:        https://yesinternet.gr/
 * Description:       Test Plugin
 * Version:           1.0.4
 * Author:            Vasilis Koutsopoulos
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       test-plugin
 * Domain Path:       /languages
 */

include 'vendor/autoload.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/vaskou/test-plugin',
	__FILE__,
	'test-plugin'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'vTBczZTQZK9QmGa8qh',
	'consumer_secret' => 'cpE6UJhj7GXDA92wSZZY2m7VearccgAb',
));

$myUpdateChecker->setBranch('master');
